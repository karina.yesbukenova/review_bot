import sqlite3


def ensure_connection(func):
    """ Декоратор для подключения к СУБД: открывает соединение,
        выполняет переданную функцию и закрывает за собой соединение.
        Потокобезопасно!
    """
    def inner(*args, **kwargs):
        with sqlite3.connect('database.db') as conn:
            kwargs['conn'] = conn
            res = func(*args, **kwargs)
        return res

    return inner


@ensure_connection
def init_db(conn, force: bool = False):
    """ Проверить что нужные таблицы существуют, иначе создать их

        Важно: миграции на такие таблицы вы должны производить самостоятельно!

        :param conn: подключение к СУБД
    """
    c = conn.cursor()

    c.execute('CREATE TABLE IF NOT EXISTS reviewer (id integer PRIMARY KEY, nick text, occupation1 text, occupation2 text, occupation3 text, service_type text, feedback_type text, promo_type text)')
    c.execute('CREATE TABLE IF NOT EXISTS blogger (id integer PRIMARY KEY, nick text, date text, price integer, ad_type text, sub_growth integer, sales_number integer, blogger_feed text, comment text, rate integer, photo text, reviewer text, FOREIGN KEY (reviewer) REFERENCES reviewer (id))')
    c.execute('CREATE TABLE IF NOT EXISTS community (id integer PRIMARY KEY, nick text, date text, price integer, ad_type text, sub_growth integer, sales_number integer, comment text, rate integer, photo text, reviewer text, FOREIGN KEY (reviewer) REFERENCES reviewer (id))')
    c.execute('CREATE TABLE IF NOT EXISTS mutual_pr (id integer PRIMARY KEY, nick text, date text, ad_type text, sub_growth integer, sales_number integer, blogger_feed text, comment text, rate integer, photo text, reviewer text, FOREIGN KEY (reviewer) REFERENCES reviewer (id))')
    c.execute('CREATE TABLE IF NOT EXISTS giv (id integer PRIMARY KEY, nick text, date text, price integer, spons_quant integer, promised_growth integer, sub_growth integer, left_sub integer, sales_number integer, blogger_feed text, comment text, rate integer, photo text, reviewer text, FOREIGN KEY (reviewer) REFERENCES reviewer (id))')

    # Сохранить изменения
    conn.commit()


@ensure_connection
def add_reviewer(conn, user: object):
    record = [user.nick, user.occupation1, user.occupation2, user.occupation3, user.service_type, user.feedback_type, user.promo_type]  # noqa
    c = conn.cursor()
    c.execute('INSERT INTO reviewer(nick, occupation1, occupation2, occupation3, service_type, feedback_type, promo_type) VALUES (?, ?, ?, ?, ?, ?, ?)', record)
    conn.commit()
    return c.lastrowid


@ensure_connection
def add_blogger(conn, blogger: object, reviewer_id: int):
    record = [
        blogger.nick, blogger.date, blogger.price, blogger.ad_type,  blogger.sub_growth, blogger.sales_number,
        blogger.blogger_feed, blogger.comment, blogger.rate[:1], ' '.join(blogger.photos), reviewer_id
    ]
    c = conn.cursor()
    c.execute('INSERT INTO blogger(nick, date, price, ad_type, sub_growth, sales_number, blogger_feed, comment, rate, photo, reviewer) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', record)
    conn.commit()


@ensure_connection
def add_community(conn, community: object, reviewer_id: int):
    record = [
        community.nick, community.date, community.price, community.ad_type,  community.sub_growth,
        community.sales_number, community.comment, community.rate[:1], ' '.join(community.photos), reviewer_id
    ]
    c = conn.cursor()
    c.execute('INSERT INTO community(nick, date, price, ad_type, sub_growth, sales_number, comment, rate, photo, reviewer) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', record)
    conn.commit()


@ensure_connection
def add_mutual_pr(conn, mutual_pr: object, reviewer_id: int):
    record = [
        mutual_pr.nick, mutual_pr.date, mutual_pr.ad_type,  mutual_pr.sub_growth, mutual_pr.sales_number,
        mutual_pr.blogger_feed, mutual_pr.comment, mutual_pr.rate[:1], ' '.join(mutual_pr.photos), reviewer_id
    ]
    c = conn.cursor()
    c.execute('INSERT INTO mutual_pr(nick, date, ad_type, sub_growth, sales_number, blogger_feed, comment, rate, photo, reviewer) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', record)
    conn.commit()


@ensure_connection
def add_giv(conn, giv: object, reviewer_id: int):
    record = [
        giv.nick, giv.date, giv.price, giv.spons_quant,  giv.promised_growth, giv.sub_growth, giv.left_sub,
        giv.sales_number, giv.blogger_feed, giv.comment, giv.rate[:1], ' '.join(giv.photos), reviewer_id
    ]
    c = conn.cursor()
    c.execute('INSERT INTO giv(nick, date, price, spons_quant, promised_growth, sub_growth, left_sub, sales_number, blogger_feed, comment, rate, photo, reviewer) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', record)
    conn.commit()