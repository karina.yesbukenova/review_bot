#!/usr/bin/env python
# -*- coding: utf-8 -*-
import telebot
import config
import logging
import sys

from telebot import types
from functools import wraps
from db import init_db, add_reviewer, add_blogger, add_community, add_mutual_pr, add_giv
from models import Reviewer, BloggerAd, CommunityAd, MutualPR, GIV
from utils import blogger_request, community_request, mutual_review, giv_review, validate_date, validate_text


init_db()

bot = telebot.TeleBot(config.TOKEN)
logging.basicConfig(stream=sys.stdout)

user_dict = {}
blogger_ad_dict = {}
community_ad_dict = {}
mutual_pr_dict = {}
giv_dict = {}


def catch_error(f):
    @wraps(f)
    def wrap(message):
        try:
            return f(message)
        except Exception as e:
            bot.send_message(message.chat.id, text='Произошла ошибка')
            logging.error("User {user} sent {message} to {func}".format(
                user=message.from_user.username, message=message.text, func=f.__name__
                ), 'Error: {}'.format(e)
            )

    return wrap


@bot.message_handler(content_types=['text'])
def start(message):
    if message.text == '/start':
        bot.send_message(
            message.chat.id,
            'Здравствуйте, {}! Напишите ваш ник в Instagram.'.format(message.from_user.username)
        )
        bot.register_next_step_handler(message, process_reviewer_nick)
    else:
        bot.send_message(message.from_user.id, 'Начните чат с команды /start')


@catch_error
def process_reviewer_nick(message):
    chat_id = message.chat.id
    user_dict[chat_id] = Reviewer(message.text)
    user = user_dict[chat_id]
    user.nick = message.text

    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add(
        'Бизнес', 'Бьюти', 'Жизнь заграницей', 'Здоровье', 'ЗОЖ', 'Изучение языков', 'Книги', 'Лайфстайл', 'Мамы',
        'Мотивация', 'Наука', 'Обработка фото', 'Образование', 'Отношения', 'Похудение', 'ПП', 'Психология', 'Авто',
        'Путешествия', 'Саморазвитие', 'Секс', 'Семья', 'Социальные темы', 'Феминизм', 'Лайфхаки', 'Бодипозитив',
        'Дом, интерьер и ремонт', 'Животные', 'Мода, стиль и шоппинг', 'Музыка', 'Природа и экология', 'Рукоделие',
        'Продуктивность', 'Профессии(юрист, дизайнер и т.д.)', 'Рецепты и еда',  'Сад и огород', 'Юмор', 'Танцы',
        'СММ, реклама и пиар', 'Спорт и фитнес', 'Творчество и искусство', 'Фильмы', 'Эзотерика', 'Другая тема'
    )
    msg = bot.send_message(
        chat_id, 'Опишите свой род деятельности в Instagram (выберите минимум 3 варианта)', reply_markup=markup
    )
    bot.register_next_step_handler(msg, process_occupation1)


@catch_error
def process_occupation1(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    user.occupation1 = message.text

    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add(
        'Бизнес', 'Бьюти', 'Жизнь заграницей', 'Здоровье', 'ЗОЖ', 'Изучение языков', 'Книги', 'Лайфстайл', 'Мамы',
        'Мотивация', 'Наука', 'Обработка фото', 'Образование', 'Отношения', 'Похудение', 'ПП', 'Психология', 'Авто',
        'Путешествия', 'Саморазвитие', 'Секс', 'Семья', 'Социальные темы', 'Феминизм', 'Лайфхаки', 'Бодипозитив',
        'Дом, интерьер и ремонт', 'Животные', 'Мода, стиль и шоппинг', 'Музыка', 'Природа и экология', 'Рукоделие',
        'Продуктивность', 'Профессии(юрист, дизайнер и т.д.)', 'Рецепты и еда',  'Сад и огород', 'Юмор', 'Танцы',
        'СММ, реклама и пиар', 'Спорт и фитнес', 'Творчество и искусство', 'Фильмы', 'Эзотерика', 'Другая тема'
    )
    msg = bot.send_message(
        chat_id, 'Выберите второй вариант *из меню*', reply_markup=markup, parse_mode='MarkdownV2'
    )
    bot.register_next_step_handler(msg, process_occupation2)


@catch_error
def process_occupation2(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    user.occupation2 = message.text

    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add(
        'Бизнес', 'Бьюти', 'Жизнь заграницей', 'Здоровье', 'ЗОЖ', 'Изучение языков', 'Книги', 'Лайфстайл', 'Мамы',
        'Мотивация', 'Наука', 'Обработка фото', 'Образование', 'Отношения', 'Похудение', 'ПП', 'Психология', 'Авто',
        'Путешествия', 'Саморазвитие', 'Секс', 'Семья', 'Социальные темы', 'Феминизм', 'Лайфхаки', 'Бодипозитив',
        'Дом, интерьер и ремонт', 'Животные', 'Мода, стиль и шоппинг', 'Музыка', 'Природа и экология', 'Рукоделие',
        'Продуктивность', 'Профессии(юрист, дизайнер и т.д.)', 'Рецепты и еда',  'Сад и огород', 'Юмор', 'Танцы',
        'СММ, реклама и пиар', 'Спорт и фитнес', 'Творчество и искусство', 'Фильмы', 'Эзотерика', 'Другая тема'
    )
    msg = bot.send_message(
        chat_id, 'Выберите третий вариант *из меню*', reply_markup=markup, parse_mode='MarkdownV2'
    )
    bot.register_next_step_handler(msg, process_occupation3)


@catch_error
def process_occupation3(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    user.occupation3 = message.text

    msg = bot.send_message(chat_id, 'Что вы рекламировали? (Например: марафон, курс, подписку и т.д)')
    bot.register_next_step_handler(msg, process_service_type)


@catch_error
def process_service_type(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    user.service_type = message.text

    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add('#положительный_отзыв', '#отрицательный_отзыв')
    msg = bot.send_message(
        chat_id, 'Какого типа отзыв вы хотите написать? \n*Выберите один из вариантов в меню*',
        reply_markup=markup, parse_mode='MarkdownV2'
    )
    bot.register_next_step_handler(msg, process_feedback_type)


@catch_error
def process_feedback_type(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    user.feedback_type = message.text

    if (user.feedback_type == u'#положительный_отзыв') or (user.feedback_type == u'#отрицательный_отзыв'):
        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        markup.add('#Реклама_у_блогера', '#Реклама_в_сообществе')
        markup.add('#Взаимный_пиар', '#Участие_в_ГИВе')
        msg = bot.send_message(
            chat_id, 'На какой вид продвижения вы хотите написать отзыв? '
                     '\n*Выберите один из вариантов в меню*', reply_markup=markup, parse_mode='MarkdownV2'
        )
        bot.register_next_step_handler(msg, process_promo_type)
    else:
        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        markup.add('#положительный_отзыв', '#отрицательный_отзыв')
        msg = bot.send_message(
            chat_id,
            'Какого типа отзыв вы хотите написать? \n*Выберите один из вариантов в меню*', reply_markup=markup,
            parse_mode='MarkdownV2'
        )
        bot.register_next_step_handler(msg, process_feedback_type)


@catch_error
def process_promo_type(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    user.promo_type = message.text

    if user.promo_type == u'#Реклама_у_блогера':
        blogger_ad_dict[chat_id] = BloggerAd(user)
        bot.send_message(chat_id, 'Напишите ник блогера')
        bot.register_next_step_handler(message, process_nick)
    elif user.promo_type == u'#Реклама_в_сообществе':
        community_ad_dict[chat_id] = CommunityAd(user)
        bot.send_message(chat_id, 'Напишите ник сообщества')
        bot.register_next_step_handler(message, process_nick)
    elif user.promo_type == u'#Взаимный_пиар':
        mutual_pr_dict[chat_id] = MutualPR(user)
        bot.send_message(chat_id, 'Напишите ник блогера')
        bot.register_next_step_handler(message, process_nick)
    elif user.promo_type == u'#Участие_в_ГИВе':
        giv_dict[chat_id] = GIV(user)
        bot.send_message(
            chat_id,
            'Напишите ник блогера, если их было несколько – *перечислить через пробел*',
            parse_mode='MarkdownV2'
        )
        bot.register_next_step_handler(message, process_nick)
    else:
        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        markup.add('#Реклама_у_блогера', '#Реклама_в_сообществе')
        markup.add('#Взаимный_пиар', '#Участие_в_ГИВе')
        msg = bot.send_message(
            chat_id, '*Выберите один из вариантов в меню*', reply_markup=markup, parse_mode='MarkdownV2')
        bot.register_next_step_handler(msg, process_promo_type)


@catch_error
def process_nick(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]

    if user.promo_type == u'#Реклама_у_блогера':
        blogger = blogger_ad_dict[chat_id]
        blogger.nick = message.text
    elif user.promo_type == u'#Реклама_в_сообществе':
        community = community_ad_dict[chat_id]
        community.nick = message.text
    elif user.promo_type == u'#Взаимный_пиар':
        mutual = mutual_pr_dict[chat_id]
        mutual.nick = message.text
    elif user.promo_type == u'#Участие_в_ГИВе':
        giv = giv_dict[chat_id]
        giv.nick = message.text

    msg = bot.send_message(
        chat_id,
        'Дата выхода рекламы /дата старта ГИВа. \n*формат даты ДД.ММ.ГГГГ*'.replace('.', "\\."),
        parse_mode='MarkdownV2'
    )
    bot.register_next_step_handler(msg, process_date_step)


@catch_error
def process_date_step(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if validate_date(message.text) == 'OK':
        if user.promo_type == u'#Реклама_у_блогера':
            blogger = blogger_ad_dict[chat_id]
            blogger.date = message.text
            msg = bot.send_message(chat_id, 'Сколько стоила реклама (в тенге)?')
            bot.register_next_step_handler(msg, process_price_step)
        elif user.promo_type == u'#Реклама_в_сообществе':
            community = community_ad_dict[chat_id]
            community.date = message.text
            msg = bot.send_message(chat_id, 'Сколько стоила реклама (в тенге)?')
            bot.register_next_step_handler(msg, process_price_step)
        elif user.promo_type == u'#Взаимный_пиар':
            mutual = mutual_pr_dict[chat_id]
            mutual.date = message.text
            markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add('сторис', 'пост')
            msg = bot.send_message(
                chat_id,
                'Вид рекламы : сторис/пост? \n*Выберите один из вариантов в меню*',
                reply_markup=markup, parse_mode='MarkdownV2'
            )
            bot.register_next_step_handler(msg, process_ad_type)
        elif user.promo_type == u'#Участие_в_ГИВе':
            giv = giv_dict[chat_id]
            giv.date = message.text
            msg = bot.send_message(chat_id, 'Стоимость гива (в тенге)')
            bot.register_next_step_handler(msg, process_price_step)
    else:
        msg = bot.send_message(
            chat_id,
            validate_text('Дата введена некорректно, попробуйте ввести ее еще раз \n*формат даты ДД.ММ.ГГГГ*'),
            parse_mode='MarkdownV2'
        )
        bot.register_next_step_handler(msg, process_date_step)


@catch_error
def process_price_step(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if message.text.isdigit():
        if user.promo_type == u'#Реклама_у_блогера':
            blogger = blogger_ad_dict[chat_id]
            blogger.price = message.text
            markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add('сторис', 'пост')
            msg = bot.send_message(
                chat_id,
                'Вид рекламы : сторис/пост? \n*Выберите один из вариантов в меню*',
                reply_markup=markup, parse_mode='MarkdownV2'
            )
            bot.register_next_step_handler(msg, process_ad_type)
        elif user.promo_type == u'#Реклама_в_сообществе':
            community = community_ad_dict[chat_id]
            community.price = message.text
            markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add('сторис', 'пост')
            msg = bot.send_message(
                chat_id,
                'Вид рекламы : сторис/пост? \n*Выберите один из вариантов в меню*',
                reply_markup=markup, parse_mode='MarkdownV2'
            )
            bot.register_next_step_handler(msg, process_ad_type)
        elif user.promo_type == u'#Участие_в_ГИВе':
            giv = giv_dict[chat_id]
            giv.price = message.text
            msg = bot.send_message(chat_id, 'Сколько было спонсоров?')
            bot.register_next_step_handler(msg, process_sponsors_quant)
    else:
        msg = bot.send_message(
            chat_id, 'Введите только численную стоимость, пожалуйста, без валюты и без сопровождающего текста'
        )
        bot.register_next_step_handler(msg, process_price_step)


@catch_error
def process_ad_type(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]

    if user.promo_type == u'#Реклама_у_блогера':
        blogger = blogger_ad_dict[chat_id]
        blogger.ad_type = message.text
        if (blogger.ad_type == u'сторис') or (blogger.ad_type == u'пост'):
            bot.send_message(chat_id, 'Какое количество подписчиков пришло?')
            bot.register_next_step_handler(message, process_sub_growth)
        else:
            markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add('сторис', 'пост')
            msg = bot.send_message(chat_id, 'Вид рекламы : сторис/пост? \n*Выберите один из вариантов в меню*',
                reply_markup=markup, parse_mode='MarkdownV2')
            bot.register_next_step_handler(msg, process_ad_type)
    elif user.promo_type == u'#Реклама_в_сообществе':
        community = community_ad_dict[chat_id]
        community.ad_type = message.text
        if (community.ad_type == u'сторис') or (community.ad_type == u'пост'):
            bot.send_message(chat_id, 'Какое количество подписчиков пришло?')
            bot.register_next_step_handler(message, process_sub_growth)
        else:
            markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add('сторис', 'пост')
            msg = bot.send_message(chat_id, 'Вид рекламы : сторис/пост? \n*Выберите один из вариантов в меню*',
                reply_markup=markup, parse_mode='MarkdownV2')
            bot.register_next_step_handler(msg, process_ad_type)
    elif user.promo_type == u'#Взаимный_пиар':
        mutual = mutual_pr_dict[chat_id]
        mutual.ad_type = message.text
        if (mutual.ad_type == u'сторис') or (mutual.ad_type == u'пост'):
            bot.send_message(chat_id, 'Какое количество подписчиков пришло?')
            bot.register_next_step_handler(message, process_sub_growth)
        else:
            markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add('сторис', 'пост')
            msg = bot.send_message(chat_id, 'Вид рекламы : сторис/пост? \n*Выберите один из вариантов в меню*',
                reply_markup=markup, parse_mode='MarkdownV2')
            bot.register_next_step_handler(msg, process_ad_type)


@catch_error
def process_sub_growth(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if message.text.isdigit():
        if user.promo_type == u'#Реклама_у_блогера':
            blogger = blogger_ad_dict[chat_id]
            blogger.sub_growth = message.text
            bot.send_message(chat_id, 'Сколько было продаж?')
            bot.register_next_step_handler(message, process_sales_number)
        elif user.promo_type == u'#Реклама_в_сообществе':
            community = community_ad_dict[chat_id]
            community.sub_growth = message.text
            bot.send_message(chat_id, 'Сколько было продаж?')
            bot.register_next_step_handler(message, process_sales_number)
        elif user.promo_type == u'#Взаимный_пиар':
            mutual = mutual_pr_dict[chat_id]
            mutual.sub_growth = message.text
            bot.send_message(chat_id, 'Сколько было продаж?')
            bot.register_next_step_handler(message, process_sales_number)
        elif user.promo_type == u'#Участие_в_ГИВе':
            giv = giv_dict[chat_id]
            giv.sub_growth = message.text
            bot.send_message(chat_id, 'Сколько осталось подписчиков от ГИВа?')
            bot.register_next_step_handler(message, process_left_sub)
    else:
        msg = bot.send_message(
            chat_id, 'Введите только численное кол-во, пожалуйста. Только число'
        )
        bot.register_next_step_handler(msg, process_sub_growth)


@catch_error
def process_sales_number(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if message.text.isdigit():
        if user.promo_type == u'#Реклама_у_блогера':
            blogger = blogger_ad_dict[chat_id]
            blogger.sales_number = message.text
            bot.send_message(chat_id, 'Опишите подачу блогера:')
            bot.register_next_step_handler(message, process_blogger_feed)
        elif user.promo_type == u'#Реклама_в_сообществе':
            community = community_ad_dict[chat_id]
            community.sales_number = message.text
            bot.send_message(chat_id, 'Ваш комментарий')
            bot.register_next_step_handler(message, process_comment)
        elif user.promo_type == u'#Взаимный_пиар':
            mutual = mutual_pr_dict[chat_id]
            mutual.sales_number = message.text
            bot.send_message(chat_id, 'Опишите подачу блогера:')
            bot.register_next_step_handler(message, process_blogger_feed)
        elif user.promo_type == u'#Участие_в_ГИВе':
            giv = giv_dict[chat_id]
            giv.sales_number = message.text
            bot.send_message(chat_id, 'Понравилась ли вам подача блогера? ')
            bot.register_next_step_handler(message, process_blogger_feed)
    else:
        msg = bot.send_message(
            chat_id, 'Введите только численное кол-во, пожалуйста. Только число'
        )
        bot.register_next_step_handler(msg, process_sales_number)


@catch_error
def process_blogger_feed(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]

    if user.promo_type == u'#Реклама_у_блогера':
        blogger = blogger_ad_dict[chat_id]
        blogger.blogger_feed = message.text
        bot.send_message(chat_id, 'Ваш комментарий')
        bot.register_next_step_handler(message, process_comment)
    elif user.promo_type == u'#Взаимный_пиар':
        mutual = mutual_pr_dict[chat_id]
        mutual.blogger_feed = message.text
        bot.send_message(chat_id, 'Ваш комментарий')
        bot.register_next_step_handler(message, process_comment)
    elif user.promo_type == u'#Участие_в_ГИВе':
        giv = giv_dict[chat_id]
        giv.blogger_feed = message.text
        bot.send_message(chat_id, 'Ваш комментарий')
        bot.register_next_step_handler(message, process_comment)


@catch_error
def process_comment(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]

    if user.promo_type == u'#Реклама_у_блогера':
        blogger = blogger_ad_dict[chat_id]
        blogger.comment = message.text
    elif user.promo_type == u'#Реклама_в_сообществе':
        community = community_ad_dict[chat_id]
        community.comment = message.text
    elif user.promo_type == u'#Взаимный_пиар':
        mutual = mutual_pr_dict[chat_id]
        mutual.comment = message.text
    elif user.promo_type == u'#Участие_в_ГИВе':
        giv = giv_dict[chat_id]
        giv.comment = message.text

    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add('1 😭', '2 😞', '3 ☹️', '4 😊', '5 😍')
    msg = bot.send_message(
        chat_id, 'Оцените рекламу \n*Выберите один из вариантов в меню*',
        reply_markup=markup, parse_mode='MarkdownV2'
    )
    bot.register_next_step_handler(msg, process_rate)


@catch_error
def process_rate(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    if message.text in ['1 😭', '2 😞', '3 ☹️', '4 😊', '5 😍']:
        if user.promo_type == u'#Реклама_у_блогера':
            blogger = blogger_ad_dict[chat_id]
            blogger.rate = message.text
        elif user.promo_type == u'#Реклама_в_сообществе':
            community = community_ad_dict[chat_id]
            community.rate = message.text
        elif user.promo_type == u'#Взаимный_пиар':
            mutual = mutual_pr_dict[chat_id]
            mutual.rate = message.text
        elif user.promo_type == u'#Участие_в_ГИВе':
            giv = giv_dict[chat_id]
            giv.rate = message.text

        msg = validate_text('Прикрепить подтверждающее фото (скрин переписки, где показано,что Вас упоминали в своих сторис, скрин поста. Если это Гив, то скрин, что на вас подписывались блогеры или скрин поста). \n\n*Отправляйте фото по одному, пожалуйста.*')  # noqa
        bot.send_message(chat_id, msg, parse_mode='MarkdownV2')
        bot.register_next_step_handler(message, process_screenshots)
    else:
        msg = bot.send_message(chat_id, '*Выберите один из вариантов в меню*', parse_mode='MarkdownV2')
        bot.register_next_step_handler(msg, process_rate)


@catch_error
def process_sponsors_quant(message):
    chat_id = message.chat.id
    giv = giv_dict[chat_id]
    giv.spons_quant = message.text

    msg = bot.send_message(chat_id, 'Какой приход подписчиков вам обещали?')
    bot.register_next_step_handler(msg, process_promised_growth)


@catch_error
def process_promised_growth(message):
    chat_id = message.chat.id
    giv = giv_dict[chat_id]
    giv.promised_growth = message.text

    msg = bot.send_message(chat_id, 'Какой приход подписчиков был по факту?')
    bot.register_next_step_handler(msg, process_sub_growth)


@catch_error
def process_left_sub(message):
    chat_id = message.chat.id
    giv = giv_dict[chat_id]
    if message.text.isdigit():
        giv.left_sub = message.text
        msg = bot.send_message(chat_id, 'Сколько было продаж?')
        bot.register_next_step_handler(msg, process_sales_number)
    else:
        msg = bot.send_message(
            chat_id, 'Введите только численное кол-во, пожалуйста. Только число'
        )
        bot.register_next_step_handler(msg, process_left_sub)


@catch_error
def process_screenshots(message):
    if message.content_type == 'photo':
        chat_id = message.chat.id
        user = user_dict[chat_id]

        if user.promo_type == u'#Реклама_у_блогера':
            blogger = blogger_ad_dict[chat_id]
            uploaded_photos = blogger.photos
            uploaded_photos += [message.photo[-1].file_id]
            blogger.photos = uploaded_photos
        elif user.promo_type == u'#Реклама_в_сообществе':
            community = community_ad_dict[chat_id]
            uploaded_photos = community.photos
            uploaded_photos += [message.photo[-1].file_id]
            community.photos = uploaded_photos
        elif user.promo_type == u'#Взаимный_пиар':
            mutual = mutual_pr_dict[chat_id]
            uploaded_photos = mutual.photos
            uploaded_photos += [message.photo[-1].file_id]
            mutual.photos = uploaded_photos
        elif user.promo_type == u'#Участие_в_ГИВе':
            giv = giv_dict[chat_id]
            uploaded_photos = giv.photos
            uploaded_photos += [message.photo[-1].file_id]
            giv.photos = uploaded_photos

        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        markup.add('Загрузить еще', 'Достаточно скринов')
        msg = bot.send_message(
            chat_id, 'Хотите ли загрузить еще скринов? *Выберите в меню из двух вариантов*',
            reply_markup=markup, parse_mode='MarkdownV2'
        )
        bot.register_next_step_handler(msg, process_one_more_photo)
    else:
        bot.reply_to(message, 'Это не фотография, пришлите пожалуйста фото.')
        bot.register_next_step_handler('', process_screenshots)


@catch_error
def process_one_more_photo(message):
    if message.text == u'Загрузить еще':
        msg = bot.send_message(message.chat.id, 'Загрузите фото/скрин')
        bot.register_next_step_handler(msg, process_screenshots)
    if message.text == u'Достаточно скринов':
        process_request(message)

        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        markup.add('/start')
        text = validate_text(
            'Отлично! Ваш отзыв сформирован!'
            '\nНапишите Аяне (*@sultan_ayana*), она проверит ваш отзыв и добавит вас в канал, где собраны все отзывы.'  # noqa
            '\n\n Чтобы приступить к созданию нового отзыва, нажмине на */start*👍👍'
        )
        msg = bot.send_message(message.chat.id, text, reply_markup=markup, parse_mode='MarkdownV2')


@catch_error
def process_request(message):
    chat_id = message.chat.id
    user = user_dict[chat_id]
    reviewer_id = add_reviewer(user=user)

    if user.promo_type == u'#Реклама_у_блогера':
        blogger = blogger_ad_dict[chat_id]
        add_blogger(blogger=blogger, reviewer_id=reviewer_id)
        review = blogger_request(user, blogger)
        bot.send_message(chat_id, review, parse_mode='MarkdownV2')
        bot.send_message(config.CHANNEL_ID, review, parse_mode='MarkdownV2')
        for photo in blogger.photos:
            bot.send_photo(config.CHANNEL_ID, photo)
        blogger_ad_dict.clear()
    elif user.promo_type == u'#Реклама_в_сообществе':
        community = community_ad_dict[chat_id]
        add_community(community=community, reviewer_id=reviewer_id)
        review = community_request(user, community)
        bot.send_message(chat_id, review, parse_mode='MarkdownV2')
        bot.send_message(config.CHANNEL_ID, review, parse_mode='MarkdownV2')
        for photo in community.photos:
            bot.send_photo(config.CHANNEL_ID, photo)
        community_ad_dict.clear()
    elif user.promo_type == u'#Взаимный_пиар':
        mutual = mutual_pr_dict[chat_id]
        add_mutual_pr(mutual_pr=mutual, reviewer_id=reviewer_id)
        review = mutual_review(user, mutual)
        bot.send_message(chat_id, review, parse_mode='MarkdownV2')
        bot.send_message(config.CHANNEL_ID, review, parse_mode='MarkdownV2')
        for photo in mutual.photos:
            bot.send_photo(config.CHANNEL_ID, photo)
        mutual_pr_dict.clear()
    elif user.promo_type == u'#Участие_в_ГИВе':
        giv = giv_dict[chat_id]
        add_giv(giv=giv, reviewer_id=reviewer_id)
        review = giv_review(user, giv)
        bot.send_message(chat_id, review, parse_mode='MarkdownV2')
        bot.send_message(config.CHANNEL_ID, review, parse_mode='MarkdownV2')
        for photo in giv.photos:
            bot.send_photo(config.CHANNEL_ID, photo)
        giv_dict.clear()


# Навесить обработчики команд
bot.enable_save_next_step_handlers(delay=1)
bot.load_next_step_handlers()

# Начать бесконечную обработку входящих сообщений
bot.polling(none_stop=True)
