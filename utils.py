import datetime


def count_sub_price(price, quant):
    if int(quant) > 0:
        price_for_sub = int(price)/int(quant)
        return round(price_for_sub, 2)
    return 0

def validate_date(date):
    try:
        datetime.datetime.strptime(date, "%d.%m.%Y")
        return 'OK'
    except ValueError:
        return 'Некорректно введеная дата'


def validate_text(text):
    chars = ['_', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!']
    for c in chars:
        if c in text:
            text = text.replace(c, "\\" + c)
    return text


def organizers(giv):
    if len(giv.nick.split(' ')) > 1:
        giv = ' ' + giv.nick
        return giv.replace(' ', ' #')
    else:
        return '#' + giv


def accs(giv):
    if len(giv.nick.split(' ')) > 1:
        links = []
        for n in giv.nick.split(' '):
            links.append('https://www.instagram.com/{}/'.format(n))
        return ', '.join(links)
    else:
        return 'https://www.instagram.com/{}/'.format(giv.nick)


def blogger_request(user, blogger):
    text = f"*{user.promo_type}  {user.feedback_type} #{blogger.ad_type}*" \
            f"\n\n💰*Цена за подписчика:* {count_sub_price(blogger.price, blogger.sub_growth)} тенге" \
           f"\n*👥Приход:* {blogger.sub_growth}" \
           f"\n*💵Цена за рекламу ({blogger.ad_type}):* {blogger.price} тенге" \
           f"\n❗️*Количество продаж:* {blogger.sales_number}" \
           f"\n⏰ *Дата выхода рекламы:* {blogger.date}" \
           f"\n\n\n🔥*Брали рекламу у:* #{blogger.nick}" \
           f"\n*Ссылка на аккаунт:* https://www.instagram.com/{blogger.nick}/" \
           f"\n\n\n*Оценка рекламы:* {blogger.rate}" \
           f"\n🔥*Мой аккаунт:* #{user.nick}" \
           f"\n*Ссылка на аккаунт:* https://www.instagram.com/{user.nick}/" \
           f"\n🎯*Тема моего аккаунта:* #{user.occupation1}, #{user.occupation2}, #{user.occupation3}" \
           f"\n*Что рекламировали:* {user.service_type}" \
           f"\n\n\n⁉️*О подаче блогера:* {blogger.blogger_feed}" \
           f"\n❗️*Комментарий:* {blogger.comment}"

    return validate_text(text)


def community_request(user, community):
    text = f"*{user.promo_type}  {user.feedback_type} #{community.ad_type}*" \
           f"\n\n💰*Цена за подписчика:* {count_sub_price(community.price, community.sub_growth)} тенге" \
           f"\n*👥Приход:* {community.sub_growth}" \
           f"\n*💵Цена за рекламу ({community.ad_type}):* {community.price} тенге" \
           f"\n❗️*Количество продаж:* {community.sales_number}" \
           f"\n⏰ *Дата выхода рекламы:* {community.date}" \
           f"\n\n\n🔥*Брали рекламу у:* #{community.nick}" \
           f"\n*Ссылка на аккаунт:* https://www.instagram.com/{community.nick}/" \
           f"\n\n\n*Оценка рекламы:* {community.rate}" \
           f"\n🔥*Мой аккаунт:* #{user.nick}" \
           f"\n*Ссылка на аккаунт:* https://www.instagram.com/{user.nick}/" \
           f"\n🎯*Тема моего аккаунта:* #{user.occupation1}, #{user.occupation2}, #{user.occupation3}" \
           f"\n*Что рекламировали:* {user.service_type}" \

    return validate_text(text)


def mutual_review(user, mutual):
    text = f"*{user.promo_type}  {user.feedback_type} #{mutual.ad_type}*" \
           f"\n\n*👥Приход:* {mutual.sub_growth}" \
           f"\n❗️*Количество продаж:* {mutual.sales_number}" \
           f"\n⏰ *Дата выхода рекламы:* {mutual.date}" \
           f"\n\n\n🔥*Брали рекламу у:* #{mutual.nick}" \
           f"\n*Ссылка на аккаунт:* https://www.instagram.com/{mutual.nick}/" \
           f"\n\n\n*Оценка рекламы:* {mutual.rate}" \
           f"\n🔥*Мой аккаунт:* #{user.nick}" \
           f"\n*Ссылка на аккаунт:* https://www.instagram.com/{user.nick}/" \
           f"\n🎯*Тема моего аккаунта:* #{user.occupation1}, #{user.occupation2}, #{user.occupation3}" \
           f"\n*Что рекламировали:* {user.service_type}" \
           f"\n\n\n⁉️*О подаче блогера:* {mutual.blogger_feed}" \
           f"\n❗️*Комментарий:* {mutual.comment}"

    return validate_text(text)


def giv_review(user, giv):
    text = f"*{user.promo_type}  {user.feedback_type}*" \
           f"\n\n*👥Приход:* {giv.sub_growth}" \
           f"\n*Какой приход обещали*: {giv.promised_growth}" \
           f"\n*Сколько осталось:* {giv.sub_growth}" \
           f"\n*💵Цена за рекламу:* {giv.price} тенге" \
           f"\n❗️*Количество продаж:* {giv.sales_number}" \
           f"\n⏰ *Дата старта ГИВа:* {giv.date}" \
           f"\n\n\n🔥*Брали рекламу у:* {organizers(giv)}" \
           f"\n*Количество спонсоров*: {giv.spons_quant}" \
           f"\n*Ссылка на аккаунт(ы):* {accs(giv)}" \
           f"\n\n\n*Оценка рекламы:* {giv.rate}" \
           f"\n🔥*Мой аккаунт:* #{user.nick}" \
           f"\n*Ссылка на аккаунт:* https://www.instagram.com/{user.nick}/" \
           f"\n🎯*Тема моего аккаунта:* #{user.occupation1}, #{user.occupation2}, #{user.occupation3}" \
           f"\n*Что рекламировали:* {user.service_type}" \
           f"\n\n\n⁉️*О подаче блогера(ов):* {giv.blogger_feed}" \
           f"\n❗️*Комментарий:* {giv.comment}"

    return validate_text(text)
