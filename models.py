class Reviewer:
    def __init__(self, nick):
        self.nick = nick
        self.occupation1 = None
        self.occupation2 = None
        self.occupation3 = None
        self.service_type = None
        self.feedback_type = None
        self.promo_type = None


class BloggerAd:
    def __init__(self, reviwer):
        self.reviewer = reviwer
        self.nick = None
        self.date = None
        self.price = None
        self.ad_type = None
        self.sub_growth = None
        self.sales_number = None
        self.blogger_feed = None
        self.comment = None
        self.rate = None
        self.photos = []


class CommunityAd:
    def __init__(self, reviewer):
        self.reviewer = reviewer
        self.nick = None
        self.date = None
        self.price = None
        self.ad_type = None
        self.sub_growth = None
        self.sales_number = None
        self.comment = None
        self.rate = None
        self.photos = []


class MutualPR:
    def __init__(self, reviewer):
        self.reviewer = reviewer
        self.nick = None
        self.date = None
        self.ad_type = None
        self.sub_growth = None
        self.sales_number = None
        self.blogger_feed = None
        self.comment = None
        self.rate = None
        self.photos = []


class GIV:
    def __init__(self, reviewer):
        self.reviewer = reviewer
        self.nick = None
        self.date = None
        self.price = None
        self.spons_quant = None
        self.promised_growth = None
        self.sub_growth = None
        self.left_sub = None
        self.sales_number = None
        self.blogger_feed = None
        self.comment = None
        self.rate = None
        self.photos = []
